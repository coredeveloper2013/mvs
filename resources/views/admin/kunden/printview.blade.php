<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'MKHYP') }}</title>

    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <!-- <link rel="stylesheet" href="/css/foundation.css" media="all" />
    <link rel="stylesheet" href="/css/new.css" media="all" /> -->
    <style>
        @import url('https://fonts.googleapis.com/css?family=Montserrat');
        * {
            /* font-family: 'Montserrat', sans-serif !important; */
            font-family: 'Roboto', sans-serif;
            /*font-family: 'Open Sans', sans-serif; */
        }
        hr {
            color: #28367b;
            max-width: 350px;
            margin: 10px auto;
        }
        #app{
            background-color: rgba(255,255,255,0.6);
            background-blend-mode: lighten;
            background: url("img/watermark.png") no-repeat right top;
        }
        body{
            margin-top:50px;
            margin-bottom:50px;
        }
        .page_title{
            /*position: absolute;
            top: -90px;
            left: 0;*/
        }
        #header { position: fixed; left: 0px; top: -40px; right: 0px; text-align: center; }
        #footer { position: fixed; left: 0px; bottom: 0px; right: 0px; height: 50px; background-color: lightblue; }
        /* .uper_box {
            top:50px;
        } */
    </style>
</head>
<body>
    <p style="text-align: center"> <img src="{{asset('img/bg_cover_sheet.jpg')}}" style="width: 600px; height: 800px;" > </p>
    <br>
    
    <p class="page_title" style="color: #28377B; font-size: 23px; margin-top: 20px;">Persönliches Angebot für<br>{{ $kunden->vorname }} {{ $kunden->nachname }}</p>
    

    <div id="header" style="margin-top: 20px;">
        <table style="width:100%;">
            <tr>
                <td style="width: 60%;"></td>
                <td><img width="200px" style="padding: 10px;" src="img/logo.png"></td>
            </tr>
        </table>
    </div>

    <div id="app">
        <div class="container" style="padding-top: 10px;"> 
            <!-- header -->
            <div style="box-shadow: 0 2px 5px rgba(0,0,0,.2);"></div>
            

            

            <!-- personal info -->
            <h3 style="color:#c8c0b3; font-size: 1.2em; margin-top: 20px"><span style="background-color: #28367b; color: #fff; padding: 10px; float: left;">Ihr Finanzierungsberater</span></h3>
            
            <div style="text-align: left; margin: 10px 0; width: 100%; line-height: 1 !important; clear: both">
                <!-- <h3 style="color: #c8c0b3;  "><span style="background-color: #28367b ; padding: 10px 20px 10px 20px;">Ihre Daten&nbsp;<span></h3> -->
                <table style="width: 100%; font-size: 12px;">
                    <tr>
                        <td>Markus Kintzelmann e. Kfm.n</td>
                        <td>Angebot ID: {{ $angebote->id }}</td>
                    </tr>
                    <tr>
                        <td>Obere Färberstraße 17a</td>
                        <td>Datum: {{ date('d.m.Y', strtotime($angebotedate->angebotdate)) }}</td>
                    </tr>
                    <tr>
                        <td>41334 Nettetal-Lobberich</td>
                    </tr>
                </table>
            </div>
            <!-- /personal info -->    

            <!-- personal info -->
            <h3 style="color:#c8c0b3; font-size: 1.2em; margin-top: 20px"><span style="background-color: #28367b ; color: #fff; padding: 10px; float: left;">Ihre Daten</span></h3>
            
            <div style="text-align: left; margin: 10px 0; width: 100%; line-height: 1 !important; clear: both">
                <!-- <h3 style="color: #c8c0b3;  "><span style="background-color: #28367b ; padding: 10px 20px 10px 20px;">Ihre Daten&nbsp;<span></h3> -->
                <table style="width: 100%; font-size: 12px;">
                    <tr>
                        <td style="text-align: left;">Vorname </td>
                        <td style="padding-left: 5px;font-weight: bold;">{{ $kunden->vorname }}</td>
                        <td style="text-align: left; padding-left: 2%;">E-mail </td>
                        <td style="padding-left: 5px;font-weight: bold;">{{ $kunden->mail }}</td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Nachname </td>
                        <td style="padding-left: 5px;font-weight: bold;">{{ $kunden->nachname }}</td>
                        <td style="text-align: left;padding-left: 2%;">Telefon </td>
                        <td style="padding-left: 5px;font-weight: bold;">{{ $kunden->telefon }}</td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Strasse </td>
                        <td style="padding-left: 5px;font-weight: bold;">{{ $kunden->strasse }}</td>
                        <td style="text-align: left;padding-left: 2%;">PLZ </td>
                        <td style="padding-left: 5px;font-weight: bold;">{{ $kunden->plz }}</td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">Geburtsdatum </td>
                        <td style="padding-left: 5px;font-weight: bold;">{{ $kunden->geburtsdatum }}</td>
                    </tr>
                </table>
            </div>
            <!-- /personal info -->        
            <h3 style="color:#c8c0b3; font-size: 1.2em; margin-top: 20px"><span style="background-color: #28367b ; color: #fff; padding: 10px; float: left;">Ubersicht</span></h3>
            <br><br><br>
            <h3 style="color:#28367b; font-size: 1.2em; margin-top: 20px; float: left; width: 100%;">Ihr Finanzierungsbedarf</h3>
            <table style="width:100%;border: 2px solid #a2a5aa;border-collapse: collapse; font-size: 12px; clear: both">
                <tr>
                    <td style="border: 1px solid #a2a5aa;padding: 4px;">Kaufpreis des Objekts</td>
                    <td style="border: 1px solid #a2a5aa;text-align: right;padding: 4px;">{{ number_format ($kunden->kaufpreis, 2, ',', '.')  }}&euro;</td>
                </tr>
                <tr>
                    <td style="border: 1px solid #a2a5aa;padding: 4px;">Umbau/Modernisierung</td>
                    <td style="border: 1px solid #a2a5aa;text-align: right;padding: 4px;">{{ number_format ($kunden->kostenumbau, 2, ',', '.')  }}&euro;</td>
                </tr>
                <tr>
                    <td style="border: 1px solid #a2a5aa;padding: 4px;">Notar/Gericht</td>
                    <td style="border: 1px solid #a2a5aa;text-align: right;padding: 4px;">{{ number_format ($kunden->kostennotar, 2, ',', '.')  }}&euro;</td>
                </tr>

                <tr>
                    <td style="border: 1px solid #a2a5aa;padding: 4px;">Grunderwerbssteuer</td>
                    <td style="border: 1px solid #a2a5aa;text-align: right;padding: 4px;">{{ number_format ($kunden->grunderwerbssteuer, 2, ',', '.')  }}&euro;</td>
                </tr>

                <tr>
                    <td style="border: 1px solid #a2a5aa;padding: 4px;">Maklerkosten</td>
                    <td style="border: 1px solid #a2a5aa;text-align: right;padding: 4px;">{{ number_format ($kunden->maklerkosten, 2, ',', '.')  }}&euro;</td>
                </tr>
                <tr style="background: #a2a5aa;font-weight: bold;">
                    <td style="padding: 4px;">Gesamtkosten</td>
                    <td style="text-align: right;padding: 4px;">{{ number_format ($kunden->gesamtkosten, 2, ',', '.') }}&euro;</td>
                </tr>
                <tr>
                    <td style="border: 1px solid #a2a5aa;padding: 4px;">Eigenkapital</td>
                    <td style="border: 1px solid #a2a5aa;text-align: right;padding: 4px;">{{ number_format($kunden->eigenkapital, 2, ',', '.') }}</td>
                </tr>
                <tr style="background: #a2a5aa;font-weight: bold;">
                    <td style="border: 1px solid #a2a5aa;padding: 4px;">Finanzierungsbedarf</td>
                    <td style="border: 1px solid #a2a5aa;text-align: right;padding: 4px;">{{ number_format($kunden->finanzierungsbedarf, 2, ',', '.') }}</td>
                </tr>
            </table>

<!--
        <div>     
            <h3 id="tilgungsplan" style="color:#28367b; font-size: 1,2em; margin-top: 50px">Tilgungsplan</h3>
            <table style="width:100%; max-height: 500px !important;border-collapse: collapse; font-size: 12px;">
                <thead>
                <tr style="background: #a2a5aa;font-weight: bold;">
                    <th style="padding: 5px 0;">Rückzahlungsdatum</th>
                    <th>Zinsen</th>
                    <th>Tilgung</th>
                    <th>Darlehensrest</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($repayments as $repayment)
                    <tr style="text-align: left;">
                        <td style="border-bottom: 1px solid #a2a5aa;padding: 3px 0">{{$repayment->repayment_date}}</td>
                        <td style="border-bottom: 1px solid #a2a5aa;padding: 3px 0">{{$repayment->zinsen}}</td>
                        <td style="border-bottom: 1px solid #a2a5aa;padding: 3px 0">{{$repayment->tilgung}}</td>
                        <td style="border-bottom: 1px solid #a2a5aa;padding: 3px 0">{{$repayment->darlehensrest}}</td>
                    </tr>
                    @endforeach
                </tbody>    
            </table>
            <br>
        </div>
        -->
        <div>
            <br><br><br>
            <h3 style="color:#28367b; font-size: 1,2em; margin-top: 50px">Ihre Finanzirungsbausteine</h3>
            <table style="width:100%; max-height: 500px !important; border-collapse: collapse; font-size: 12px;">
                <thead>
                <tr style="background: #a2a5aa;font-weight: bold;">
                    <th></th>
                    {{-- <th>Annuities</th>
                    <th>To Interest</th>
                    <th>Effectiveness</th>
                    <th>Fixed Interest Rates</th>
                    <th>Monthly Loan</th>
                    <th>Residual Debt Interest Rate</th>
                    <th>Calculated Luaf Time</th>
                    <th>Net Loan Amount</th>
                    <th>Initial Interest</th>
                    <th>Optional Sound Recovery</th> --}}
                </tr>
                </thead>
                <tbody>
                    @php $i = 1; @endphp
                    @foreach( $Calculations as $calculation )
                        {{-- <tr>
                            <td style="text-align: left; border-bottom: 1px solid #a2a5aa;padding: 3px 0">{{$calculation->bank}}</td>
                            <td style="text-align: left; border-bottom: 1px solid #a2a5aa;padding: 3px 0">{{$calculation->annuities}}</td>
                            <td style="text-align: left; border-bottom: 1px solid #a2a5aa;padding: 3px 0">{{$calculation->to_interest}}</td>
                            <td style="text-align: left; border-bottom: 1px solid #a2a5aa;padding: 3px 0">{{$calculation->effectiveness}}</td>
                            <td style="text-align: left; border-bottom: 1px solid #a2a5aa;padding: 3px 0">{{$calculation->fixed_interest_rates}}</td>
                            <td style="text-align: left; border-bottom: 1px solid #a2a5aa;padding: 3px 0">{{$calculation->monthly_loan}}</td>
                            <td style="text-align: left; border-bottom: 1px solid #a2a5aa;padding: 3px 0">{{$calculation->residual_debt_interest_rate}}</td>
                            <td style="text-align: left; border-bottom: 1px solid #a2a5aa;padding: 3px 0">{{$calculation->calculated_luaf_time}}</td>
                            <td style="text-align: left; border-bottom: 1px solid #a2a5aa;padding: 3px 0">{{$calculation->net_loan_amount}}</td>
                            <td style="text-align: left; border-bottom: 1px solid #a2a5aa;padding: 3px 0">{{$calculation->initial_interest}}</td>
                            <td style="text-align: left; border-bottom: 1px solid #a2a5aa;padding: 3px 0">{{$calculation->optional_sound_recovery}}</td>
                        </tr> --}}
                        <tr><td><div>{{ '#Finanzirungsbaustein ' . $i }}</div><span style="float:left; width: 200px">Bank</span><span style="text-align: right">{{$calculation->bank}}</span></td></tr>
                        <tr><td><span style="float:left; width: 200px">Annuities</span><span style="text-align: right">{{$calculation->annuities}}</span></td></tr>
                        <tr><td><span style="float:left; width: 200px">To Interest</span><span style="text-align: right">{{$calculation->to_interest}}</span></td></tr>
                        <tr><td><span style="float:left; width: 200px">Effectiveness</span><span style="text-align: right">{{$calculation->effectiveness}}</span></td></tr>
                        <tr><td><span style="float:left; width: 200px">Fixed Interest Rates</span><span style="text-align: right">{{$calculation->fixed_interest_rates}}</span></td></tr>
                        <tr><td><span style="float:left; width: 200px">Monthly Loan</span><span style="text-align: right">{{$calculation->monthly_loan}}</span></td></tr>
                        <tr><td><span style="float:left; width: 200px">Residual Debt Interest Rate</span><span style="text-align: right">{{$calculation->residual_debt_interest_rate}}</span></td></tr>
                        <tr><td><span style="float:left; width: 200px">Calculated Luaf Time</span><span style="text-align: right">{{$calculation->calculated_luaf_time}}</span></td></tr>
                        <tr><td><span style="float:left; width: 200px">Net Loan Amount</span><span style="text-align: right">{{$calculation->net_loan_amount}}</span></td></tr>
                        <tr><td><span style="float:left; width: 200px">Initial Interest</span><span style="text-align: right">{{$calculation->initial_interest}}</span></td></tr>
                        <tr><td><span style="float:left; width: 200px">Optional Sound Recovery</span><span style="text-align: right">{{$calculation->optional_sound_recovery}}</span></td></tr>
                        @if ( $calculation->timeline != null )
                            <tr>
                                <td>
                                    <div class="uper_box">
                                        <div class="container">
                                            <h3 style="color: #28367b; margin-top:15px;">Übersicht</h3>
                                            <table style="width: 100%; font-size: 18px;" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style="font-size:10px">{{ $calculation->timeline->finanzierungsbedarf_phase_eins }}&euro;</td>
                                                    <td style="border-left: 4px solid #f1ac38; border-right: 4px solid #f1ac38;">
                                                        <table style="width: 100%; border-spacing: 0">
                                                            <tr style="border: 1px solid #f1ac38; height: 50px; width: 100%;">
                                                                <td style="text-align: center;border-bottom: 4px solid #f1ac38; width: 25%;">
                                                                    <p style="margin-top:0;margin-bottom:10px;font-size:10px">{{ $calculation->timeline->laufzeit_phase_eins }} Jahre</p>
                                                                </td>
                                                                <td style="text-align: center;border-bottom: 4px solid #f1ac38; width: 13%;">
                                                                    <p style="margin-top:0;margin-bottom:10px;font-size:10px">{{ $calculation->timeline->jahreszins_phase_eins }}%</p>
                                                                </td>
                                                                <td style="text-align: center;border-bottom: 4px solid #f1ac38; width: 25%;">
                                                                    <p style="margin-top:0;margin-bottom:20px;font-size:10px">dann</p>
                                                                </td>
                                                                {{-- <td style="text-align: center;border-bottom: 4px solid #f1ac38; width: 0%;">
                                                                    <p style="margin-top:0;margin-bottom:10px;font-size:10px"></p>
                                                                </td> --}}
                                                                <td style="text-align: center;border-bottom: 4px solid #f1ac38; width: 25%;">
                                                                    <p style="margin-top:0;margin-bottom:10px;font-size:10px">{{ $calculation->timeline->laufzeit_phase_zwei }} Jahre</p>
                                                                </td>
                                                                <td style="text-align: center;border-bottom: 4px solid #f1ac38; width: 12%;">
                                                                    <p style="margin-top:0;margin-bottom:10px;font-size:10px">{{ $calculation->timeline->jahreszins_phase_zwei }}%</p>
                                                                </td>
                                                            </tr>
                                                            <tr style="border: 4px solid #f1ac38; width: 100%; height: 70px;">
                                                                <td style="text-align: center;width: 33.334%;font-size:10px" colspan="2">{{ $calculation->timeline->rate_monatlich_phase_eins }}&euro;</td>
                                                                <td width="33%" align="center" style="position: relative;">
                                                                    <div style="height:26px;padding-top: 10px;font-size:10px"> -> <br>Restschuld <br>{{ $calculation->timeline->restschuld_phase_eins }}&euro;</div>
                                                                    <span style="display: inline-block; width: 5px; height: 27px; border-right:4px solid #f1ac38; position: absolute;  z-index:99;top: -18px;left: 47%;"></span>
                                                                </td>
                                                                <td style="text-align: center; width: 33.334%;font-size:10px">{{ $calculation->timeline->rate_monatlich_phase_zwei }}&euro;</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td style="text-align: center;font-size:10px">30 jahre</td>
                                                    <td style="text-align: center;"><span style="text-align: center; padding:5px; display: block; border: 4px solid #f1ac38;font-size:10px">Restschuld <br>{{ $calculation->timeline->restschuld_ende }}&euro;</span></td>
                                                </tr>
                                            </table>
                                        </div>  
                                    </div>
                                    <br><br><br><br><br>
                                </td>
                            </tr>
                        @else 
                            <tr><td><strong>No timeline data found</strong></td></tr>
                            <tr><td></td></tr>
                            <tr><td></td></tr>
                            <tr><td></td></tr>
                            <tr><td></td></tr>
                            <tr><td></td></tr>
                            <tr><td></td></tr>
                            <tr><td></td></tr>
                            <tr><td></td></tr>
                            <tr><td></td></tr>
                            <tr><td></td></tr>
                            {{-- <tr>
                                <td><strong>No timeline data found</strong></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr> --}}
                        @endif
                        @php $i++; @endphp
                    @endforeach
                </tbody>    
            </table>
        </div>
    </div>
</body>
</html>
